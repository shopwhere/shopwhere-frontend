import Footer from "./Footer";
import Navbar from "./Navbar";
import Head from "next/head";

const Layout = ({ children, categories }) => {
  return (
    <div className="js">
      <Navbar />
      {/*<CategoryButtons categories={categories} />*/}
      <div>{children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
