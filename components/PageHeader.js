import Head from "next/head";
import React from "react";
import HomePage from '../pages'

const PageHeader = ({ title, description }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />

      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <link rel="icon" href="/images/favicon.ico" />
      <meta name="copyright" content="" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap"
        rel="stylesheet"
      />

      <link rel="stylesheet" href="/css/bootstrap.css" />
      <link rel="stylesheet" href="/css/magnific-popup.min.css" />
      <link rel="stylesheet" href="/css/font-awesome.css" />
      <link rel="stylesheet" href="/css/jquery.fancybox.min.css" />
      <link rel="stylesheet" href="/css/themify-icons.css" />
      <link rel="stylesheet" href="/css/niceselect.css" />
      <link rel="stylesheet" href="/css/animate.css" />
      <link rel="stylesheet" href="/css/flex-slider.min.css" />
      <link rel="stylesheet" href="/css/owl-carousel.css" />
      <link rel="stylesheet" href="/css/slicknav.min.css" />
      <link rel="stylesheet" href="/css/reset.css" />
      <link rel="stylesheet" href="/style.css" />
      <link rel="stylesheet" href="/css/responsive.css" />
    </Head>
  );
};

export default PageHeader;
