import React from "react";

const Navbar = () => {
  return (
    <div>
      <div className="header shop">
        <div className="middle-inner">
          <div className="container">
            <div className="row">
              <div className="col-lg-2 col-md-2 col-12">
                {/* Logo */}
                <div className="logo">
                  <a href="index.html">
                    <img src="images/logo.png" alt="logo" />
                  </a>
                </div>
                {/* End Logo Search Form */}
                <div className="search-top">
                  <div className="top-search">
                    <a href="#0">
                      <i className="ti-search" />
                    </a>
                  </div>
                  {/* Search Form */}
                  <div className="search-top">
                    <form className="search-form">
                      <input
                        type="text"
                        placeholder="Search here..."
                        name="search"
                      />
                      <button value="search" type="submit">
                        <i className="ti-search" />
                      </button>
                    </form>
                  </div>
                  {/* End Search Form */}
                </div>
                {/* End Search Form */}
                <div className="mobile-nav">
                  <div className="slicknav_menu">
                    <a
                      href="#"
                      aria-haspopup="true"
                      role="button"
                      tabIndex="0"
                      className="slicknav_btn slicknav_collapsed"
                    >
                      <span className="slicknav_menutxt" />
                      <span className="slicknav_icon slicknav_no-text">
                        <span className="slicknav_icon-bar" />
                        <span className="slicknav_icon-bar" />
                        <span className="slicknav_icon-bar" />
                      </span>
                    </a>
                    <ul
                      className="slicknav_nav slicknav_hidden"
                      aria-hidden="true"
                      role="menu"
                      style={{ display: "none" }}
                    >
                      <li className="active">
                        <a href="#" role="menuitem" tabIndex="-1">
                          Home
                        </a>
                      </li>
                      <li>
                        <a href="#" role="menuitem" tabIndex="-1">
                          Product
                        </a>
                      </li>
                      <li>
                        <a href="#" role="menuitem" tabIndex="-1">
                          Service
                        </a>
                      </li>
                      <li className="slicknav_collapsed slicknav_parent">
                        <a
                          href="#"
                          role="menuitem"
                          aria-haspopup="true"
                          tabIndex="-1"
                          className="slicknav_item slicknav_row"
                        >
                          <a href="#" tabIndex="-1">
                            Shop
                            <i className="ti-angle-down" />
                            <span className="new">New</span>
                          </a>
                          <span className="slicknav_arrow">►</span>
                        </a>
                        <ul
                          className="dropdown slicknav_hidden"
                          role="menu"
                          aria-hidden="true"
                          style={{ display: "none" }}
                        >
                          <li>
                            <a
                              href="shop-grid.html"
                              role="menuitem"
                              tabIndex="-1"
                            >
                              Shop Grid
                            </a>
                          </li>
                          <li>
                            <a href="cart.html" role="menuitem" tabIndex="-1">
                              Cart
                            </a>
                          </li>
                          <li>
                            <a
                              href="checkout.html"
                              role="menuitem"
                              tabIndex="-1"
                            >
                              Checkout
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a href="#" role="menuitem" tabIndex="-1">
                          Pages
                        </a>
                      </li>
                      <li className="slicknav_collapsed slicknav_parent">
                        <a
                          href="#"
                          role="menuitem"
                          aria-haspopup="true"
                          tabIndex="-1"
                          className="slicknav_item slicknav_row"
                        >
                          <a href="#" tabIndex="-1">
                            Blog
                            <i className="ti-angle-down" />
                          </a>
                          <span className="slicknav_arrow">►</span>
                        </a>
                        <ul
                          className="dropdown slicknav_hidden"
                          role="menu"
                          aria-hidden="true"
                          style={{ display: "none" }}
                        >
                          <li>
                            <a
                              href="blog-single-sidebar.html"
                              role="menuitem"
                              tabIndex="-1"
                            >
                              Blog Single Sidebar
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a href="contact.html" role="menuitem" tabIndex="-1">
                          Contact Us
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-8 col-md-7 col-12">
                <div className="search-bar-top">
                  <div className="search-bar">
                    <select style={{ display: "none" }}>
                      <option selected="selected">Tìm kiếm tất cả</option>
                      <option>Tìm kiếm cửa hàng</option>
                      <option>Tìm kiếm sản phẩm</option>
                    </select>
                    <div className="nice-select" tabIndex="0">
                      <span className="current">Tất cả</span>
                      <ul className="list">
                        <li data-value="all" className="option selected">
                          Tất cả
                        </li>
                        <li data-value="shop" className="option">
                          Cửa hàng
                        </li>
                        <li data-value="item" className="option">
                          Sản phẩm
                        </li>
                      </ul>
                    </div>
                    <form>
                      <input
                        name="search"
                        placeholder="Tìm kiếm..."
                        type="search"
                      />
                      <button className="btnn">
                        <i className="ti-search" />
                      </button>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-lg-2 col-md-3 col-12">
                <div className="right-bar">
                  {/* Search Form */}
                  <div className="sinlge-bar">
                    <a href="#" className="single-icon">
                      <i className="fa fa-heart-o" aria-hidden="true" />
                    </a>
                  </div>
                  <div className="sinlge-bar">
                    <a href="#" className="single-icon">
                      <i className="fa fa-user-circle-o" aria-hidden="true" />
                    </a>
                  </div>
                  <div className="sinlge-bar shopping">
                    <a href="#" className="single-icon">
                      <i className="ti-bag" />{" "}
                      <span className="total-count">2</span>
                    </a>
                    {/* Shopping Item */}
                    <div className="shopping-item">
                      <div className="dropdown-cart-header">
                        <span>2 Items</span>
                        <a href="#">View Cart</a>
                      </div>
                      <ul className="shopping-list">
                        <li>
                          <a
                            href="#"
                            className="remove"
                            title="Remove this item"
                          >
                            <i className="fa fa-remove" />
                          </a>
                          <a className="cart-img" href="#">
                            <img
                              src="https://via.placeholder.com/70x70"
                              alt="#"
                            />
                          </a>
                          <h4>
                            <a href="#">Woman Ring</a>
                          </h4>
                          <p className="quantity">
                            1x - <span className="amount">$99.00</span>
                          </p>
                        </li>
                        <li>
                          <a
                            href="#"
                            className="remove"
                            title="Remove this item"
                          >
                            <i className="fa fa-remove" />
                          </a>
                          <a className="cart-img" href="#">
                            <img
                              src="https://via.placeholder.com/70x70"
                              alt="#"
                            />
                          </a>
                          <h4>
                            <a href="#">Woman Necklace</a>
                          </h4>
                          <p className="quantity">
                            1x - <span className="amount">$35.00</span>
                          </p>
                        </li>
                      </ul>
                      <div className="bottom">
                        <div className="total">
                          <span>Total</span>
                          <span className="total-amount">$134.00</span>
                        </div>
                        <a href="checkout.html" className="btn animate">
                          Checkout
                        </a>
                      </div>
                    </div>
                    {/* End Shopping Item */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Header Inner */}
        <div className="header-inner">
          <div className="container">
            <div className="cat-nav-head">
              <div className="row">
                <div className="col-lg-3">
                  <div className="all-category">
                    <h3 className="cat-heading">
                      <i className="fa fa-bars" aria-hidden="true" />
                      CATEGORIES
                    </h3>
                    <ul className="main-category">
                      <li>
                        <a href="#">
                          New Arrivals{" "}
                          <i className="fa fa-angle-right" aria-hidden="true" />
                        </a>
                        <ul className="sub-category">
                          <li>
                            <a href="#">accessories</a>
                          </li>
                          <li>
                            <a href="#">best selling</a>
                          </li>
                          <li>
                            <a href="#">top 100 offer</a>
                          </li>
                          <li>
                            <a href="#">sunglass</a>
                          </li>
                          <li>
                            <a href="#">watch</a>
                          </li>
                          <li>
                            <a href="#">man’s product</a>
                          </li>
                          <li>
                            <a href="#">ladies</a>
                          </li>
                          <li>
                            <a href="#">westrn dress</a>
                          </li>
                          <li>
                            <a href="#">denim </a>
                          </li>
                        </ul>
                      </li>
                      <li className="main-mega">
                        <a href="#">
                          best selling{" "}
                          <i className="fa fa-angle-right" aria-hidden="true" />
                        </a>
                        <ul className="mega-menu">
                          <li className="single-menu">
                            <a href="#" className="title-link">
                              Shop Kid's
                            </a>
                            <div className="image">
                              <img
                                src="https://via.placeholder.com/225x155"
                                alt="#"
                              />
                            </div>
                            <div className="inner-link">
                              <a href="#">Kids Toys</a>
                              <a href="#">Kids Travel Car</a>
                              <a href="#">Kids Color Shape</a>
                              <a href="#">Kids Tent</a>
                            </div>
                          </li>
                          <li className="single-menu">
                            <a href="#" className="title-link">
                              Shop Men's
                            </a>
                            <div className="image">
                              <img
                                src="https://via.placeholder.com/225x155"
                                alt="#"
                              />
                            </div>
                            <div className="inner-link">
                              <a href="#">Watch</a>
                              <a href="#">T-shirt</a>
                              <a href="#">Hoodies</a>
                              <a href="#">Formal Pant</a>
                            </div>
                          </li>
                          <li className="single-menu">
                            <a href="#" className="title-link">
                              Shop Women's
                            </a>
                            <div className="image">
                              <img
                                src="https://via.placeholder.com/225x155"
                                alt="#"
                              />
                            </div>
                            <div className="inner-link">
                              <a href="#">Ladies Shirt</a>
                              <a href="#">Ladies Frog</a>
                              <a href="#">Ladies Sun Glass</a>
                              <a href="#">Ladies Watch</a>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a href="#">accessories</a>
                      </li>
                      <li>
                        <a href="#">top 100 offer</a>
                      </li>
                      <li>
                        <a href="#">sunglass</a>
                      </li>
                      <li>
                        <a href="#">watch</a>
                      </li>
                      <li>
                        <a href="#">man’s product</a>
                      </li>
                      <li>
                        <a href="#">ladies</a>
                      </li>
                      <li>
                        <a href="#">westrn dress</a>
                      </li>
                      <li>
                        <a href="#">denim </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-9 col-12">
                  <div className="menu-area">
                    {/* Main Menu */}
                    <nav className="navbar navbar-expand-lg">
                      <div className="navbar-collapse">
                        <div className="nav-inner">
                          <ul className="nav main-menu menu navbar-nav">
                            <li className="active">
                              <a href="#">Home</a>
                            </li>
                            <li>
                              <a href="#">Product</a>
                            </li>
                            <li>
                              <a href="#">Service</a>
                            </li>
                            <li>
                              <a href="#">
                                Shop
                                <i className="ti-angle-down" />
                                <span className="new">New</span>
                              </a>
                              <ul className="dropdown">
                                <li>
                                  <a href="shop-grid.html">Shop Grid</a>
                                </li>
                                <li>
                                  <a href="cart.html">Cart</a>
                                </li>
                                <li>
                                  <a href="checkout.html">Checkout</a>
                                </li>
                              </ul>
                            </li>
                            <li>
                              <a href="#">Pages</a>
                            </li>
                            <li>
                              <a href="#">
                                Blog
                                <i className="ti-angle-down" />
                              </a>
                              <ul className="dropdown">
                                <li>
                                  <a href="blog-single-sidebar.html">
                                    Blog Single Sidebar
                                  </a>
                                </li>
                              </ul>
                            </li>
                            <li>
                              <a href="contact.html">Contact Us</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </nav>
                    {/* End Main Menu */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* End Header Inner */}
      </div>
      <section className="hero-slider">
        {/* Single Slider */}
        <div className="single-slider">
          <div className="container">
            <div className="row no-gutters">
              <div className="col-lg-9 offset-lg-3 col-12">
                <div className="text-inner">
                  <div className="row">
                    <div className="col-lg-7 col-12">
                      <div className="hero-text">
                        <h1>
                          <span>UP TO 50% OFF </span>Shirt For Man
                        </h1>
                        <p>
                          Maboriosam in a nesciung eget magnae <br /> dapibus
                          disting tloctio in the find it pereri <br /> odiy
                          maboriosm.
                        </p>
                        <div className="button">
                          <a href="#" className="btn">
                            Shop Now!
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*/ End Single Slider */}
      </section>
      {/*/ End Slider Area */}
    </div>
  );
};

export default Navbar;
